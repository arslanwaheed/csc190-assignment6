/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author arslanwaheed
 */
public class Client {

    public static void main(String[] args) {
        
        String datastr = "";
        String url = "http://10.22.13.87/assignment6.php";
        
        if(args[0].equalsIgnoreCase("uploadData")){
            //read xml file and serialize data
            EmployeesArray empArray = buildEmployeesArray();
            String empArrStr = Utils.toStr(empArray);
            
            //update datastr
            datastr = "op=uploadData&str=" + empArrStr;
        }else{
            datastr = "op=getAvailableEmployees&time="+ args[0] + "&skill=" + args[1]; 
        }
        
        try{
            String response = Utils.httpsPost(url, datastr);
            if(response.contains("Success!")){
                System.out.println(response);
            }else {
                //because response string has some characters in it that needs to be changed
                //'+' to "%2B"
                //'/' to "%2f"
                //'=' to "%3D"
                //the loop replaces all of them and puts it in r
                //and also we need remove last char from r string which is endline character
                //after all the processing it will work fine, other will give error
                String r = "";
                for(int i=0; i<response.length(); i++){
                    if(response.charAt(i) == '+'){
                        r = r + "%2B";
                    }
                    else if(response.charAt(i) == '/'){
                        r = r + "%2F";
                    }
                    else if(response.charAt(i) == '='){
                        r = r + "%3D";
                    }
                    else{
                        r = r + response.charAt(i);
                    }
                }
                r = r.substring(0, r.length()-1);
                
                //create list of employees by decoding
                EmployeesArray empArray = new EmployeesArray();
                empArray = (EmployeesArray)(Utils.toObj(r));
                
                //see which employees are available
                EmployeesArray availableEmployees = new EmployeesArray();
                TimeSlot ts = new TimeSlot(args[0]);
                String skill = args[1];
                availableEmployees = empArray.getAvailableEmployee(skill, ts);
                
                if(availableEmployees.size() != 0){
                    for(Employee e : availableEmployees){
                    System.out.println(e.name);
                    }
                }else {
                    System.out.println("No Employees Available.");
                }
                
            }    
        }
        catch(Exception ex){
            System.out.println("Exception caught : "+ ex);
        }
    }

    //build employeeArray
    public static EmployeesArray buildEmployeesArray(){
        EmployeesArray empArray = new EmployeesArray();
        //this try and catch block will read employee data from xml file and 
        //will populate empArray using it
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();              
            Document doc = dBuilder.parse("src/assignment6/employeeData.xml");
            
            doc.getDocumentElement().normalize();
            
            NodeList nList = doc.getElementsByTagName("employee");
            
            int length = nList.getLength();

            for(int j=0; j<length; j++) {
                Node node = nList.item(j);
				
                if(node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    
                    //reading name and initializing Employee
                    String name = eElement.getElementsByTagName("name").item(0).getTextContent();
                    Employee emp = new Employee(name);
  
                    //reading skills
                    int skillLength = eElement.getElementsByTagName("skill").getLength();
                    for(int i=0; i< skillLength ; i++){
                        String skill = eElement.getElementsByTagName("skill").item(i).getTextContent();
                        emp.addSkill(skill);
                    }
                    
                    //reading availbale timeslots
                    int timeLength = eElement.getElementsByTagName("timeslot").getLength();
                    for(int i=0; i< timeLength ; i++){
                        String time = eElement.getElementsByTagName("timeslot").item(i).getTextContent();
                        TimeSlot t = new TimeSlot(time);
                        emp.addTime(t);
                    }
                    
                    //adding Employee to ArrayList
                    empArray.add(emp);
                }    
            }          
        }
        catch(Exception ex){
            System.out.println("Exception caught: " + ex + "\n");
        }
        
        return empArray;
    }
    
}
