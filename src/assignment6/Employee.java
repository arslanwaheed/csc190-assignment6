/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

import java.util.ArrayList;

/**
 *
 * @author arslanwaheed
 */
public class Employee implements java.io.Serializable{
    String name;
    int id;
    ArrayList<String> skills;
    ArrayList<TimeSlot> time;
    
    public Employee(String n){
        skills = new ArrayList<>();
        name = n;
        id = generateID();
        time = new ArrayList<>();
    }
    
    public void addSkill(String s){
        skills.add(s);
    }
    
    public void addTime(TimeSlot t){
        time.add(t);
    }
    
    private int generateID(){
        int num;
        num = (int)(Math.random()*999999999);
        return num;
    }
    
    @Override
    public String toString(){
        String str;
        str = "\nName: " + name + "\nID: " + id +"\nSkills: " + skills +
                "\nTimeSlot: " + time +"\n";
        return str;
    }
    
}
