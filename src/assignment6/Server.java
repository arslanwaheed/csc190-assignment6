/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

/**
 *
 * @author arslanwaheed
 */
public class Server {
    public static void main(String[] args) {
        if(args[0].equalsIgnoreCase("uploadData")){
            updateEmpArrData(args[1]);
            System.out.println("Success!");

        }else{
            //get all employees from database
            //processing will be done on client side
            String empArrObjStr =  getEmployees();
            System.out.println(empArrObjStr);
        }
    }
    
    protected static void updateEmpArrData(String val){
        String qry = "UPDATE employees SET byteStr='" + val +"'";
        Utils.execNonQuery(qry);
    }
    
    protected static String getEmployees(){   
        String qry = "SELECT byteStr FROM employees";
        String empArrObjStr = Utils.execQuery(qry);
        return empArrObjStr;
    }   
}