/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

import java.util.ArrayList;

/**
 *
 * @author arslanwaheed
 */
public class EmployeesArray extends ArrayList<Employee> implements java.io.Serializable{
    ArrayList<Employee> employees;
    
    public EmployeesArray(){
        employees = new ArrayList<>();
    }
    
    public EmployeesArray getAvailableEmployee(String skill, TimeSlot t){
        EmployeesArray emps = new EmployeesArray();
        skill = skill.toLowerCase();
        
        for(int i=0; i< this.size(); i++){
            Employee e = this.get(i);

            if((e.skills.contains(skill))){
                for(TimeSlot ts: e.time){
                    if(ts.hour == t.hour && ts.weekday.equalsIgnoreCase(t.weekday)){
                        emps.add(e);
                    }
                }
            }
        }
        return emps;
    }
}
