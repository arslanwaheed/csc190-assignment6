/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment6;

/**
 *
 * @author arslanwaheed
 */
public class TimeSlot implements java.io.Serializable{
    String weekday;
    int hour;
  
    /*
    public TimeSlot(){
        weekday = "";
        hour = 0;
    }
    */
    
    public TimeSlot(String s){
        weekday = "" + (s.charAt(0));
        weekday = weekday.toUpperCase();
        hour = Integer.parseInt(s.substring(1));
    }
    
    @Override
    public String toString(){
        String str;
        str ="\nWeekday: " + weekday + "\tHour: "+ hour + " ";
        
        return str;
    }
}
